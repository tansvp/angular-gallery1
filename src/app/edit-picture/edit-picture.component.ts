import { Component, OnInit } from '@angular/core';
import {PictureService} from "../picture.service";
import {Router} from "@angular/router";
import {Picture} from "../picture";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {first} from "rxjs/operators";

@Component({
  selector: 'app-edit-picture',
  templateUrl: './edit-picture.component.html',
  styleUrls: ['./edit-picture.component.css']
})
export class EditPictureComponent implements OnInit {

  picture: Picture;
  editForm: FormGroup;
  constructor(private formBuilder: FormBuilder,private router: Router, private pictureService: PictureService) { }

  ngOnInit() {
    let pictureId = localStorage.getItem("editPictureId");
    if(!pictureId) {
      alert("Invalid action.")
      this.router.navigate(['pictures']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: [],
      detail: ['', Validators.required],
      name : ['', Validators.required],
      camera: ['', Validators.required],
    });
    this.pictureService.getPictureById(+pictureId)
      .subscribe( data => {
        this.editForm.setValue(data);
      });
  }

  onSubmit() {
    this.pictureService.updatePicture(this.editForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['pictures']);
        },
        error => {
          alert(error);
        });
  }

}