

/* import { Component, OnInit } from '@angular/core';
import { Picture } from '../picture';
import { PictureService } from '../picture.service';

@Component({
  selector: 'app-pictures',
  templateUrl: './pictures.component.html',
  styleUrls: ['./pictures.component.css']
})
export class PicturesComponent implements OnInit {


  pictures: Picture[];

  constructor(
    private pictureService: PictureService, 
    ) { }

  ngOnInit() {
    this.getPictures();
  }

  getPictures(): void {
    this.pictureService.getPictures()
        .subscribe(pictures => this.pictures = pictures);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.pictureService.addPicture({ name } as Picture)
      .subscribe(picture => {
        this.pictures.push(picture);
      });
  }

  delete(picture: Picture): void {
    this.pictures = this.pictures.filter(h => h !== picture);
    this.pictureService.deletePicture(picture).subscribe();
  }


}
 */

import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Picture }         from '../picture';
import { PictureService }  from '../picture.service';

@Component({
  selector: 'app-pictures',
  templateUrl: './pictures.component.html',
  styleUrls: ['./pictures.component.css']
})
export class PicturesComponent implements OnInit {
  pictures: Picture[];
  /* เอารูปทั้งหมดมาใส่ */

  /* SEA&SKY */
  myimage1:string = "assets/image/1.1.jpg"; 
  myimage2:string = "assets/image/1.5.jpg";
  myimage3:string = "assets/image/1.3.jpg";
  myimage4:string = "assets/image/1.6.jpg";
  myimage5:string = "assets/image/6.2.jpg";
  myimage6:string = "assets/image/6.1.jpg";  

  /* MOON */
  myimage7:string = "assets/image/2.11.jpg"; 
  myimage8:string = "assets/image/2.2.jpg";  
  myimage9:string = "assets/image/2.3.jpg";  
  myimage10:string = "assets/image/2.4.jpg";  
  myimage11:string = "assets/image/2.5.jpg";  

  /* MOUNTAIN */
  myimage12:string = "assets/image/3.11.jpg";  
  myimage13:string = "assets/image/3.22.jpg";  
  myimage14:string = "assets/image/3.66.jpg";
  myimage16:string = "assets/image/3.444.jpg";
  myimage15:string = "assets/image/3.77.jpg";  
  myimage17:string = "assets/image/3.99.jpg";  

  /* MUSEUM */
  myimage18:string = "assets/image/a.jpg" ;  
  myimage19:string = "assets/image/b.jpg";  
  myimage20:string = "assets/image/c.jpg";  
  myimage21:string = "assets/image/gg.jpg"; 
  myimage22:string = "assets/image/e.jpg";
  myimage23:string = "assets/image/f.jpg";  
  myimage24:string = "assets/image/d.jpg"; 
  myimage38:string = "assets/image/h.jpg";  
  myimage35:string = "assets/image/i.jpg";  
  myimage37:string = "assets/image/k.jpg"; 
  myimage36:string = "assets/image/s.jpg";  
 


  
  /* TEMPLE */
  myimage31:string = "assets/image/00000.jpg";  
  myimage32:string = "assets/image/11111.jpg";  
  myimage25:string = "assets/image/6666.jpg";  
  myimage26:string = "assets/image/1111.jpg";  
  myimage27:string = "assets/image/2222.jpg";
  myimage28:string = "assets/image/55555.jpg";  
  myimage29:string = "assets/image/22222.jpg"; 
  myimage30:string = "assets/image/33333.jpg";  
  myimage33:string = "assets/image/44444.jpg";  
  myimage34:string = "assets/image/5555.jpg";  


  constructor(
    private route: ActivatedRoute,
    private pictureService: PictureService,
    private location: Location,
    private router: Router,



  ) {}

  ngOnInit() {
    this.getPictures();
  }

  getPictures(): void {
    this.pictureService.getPictures()
        .subscribe(pictures => this.pictures = pictures);
  }

 



  /*   
   add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.pictureService.addPicture({ name } as Picture)
      .subscribe(picture => {
        this.pictures.push(picture);
      });
  }
  
  adddd(detail: string): void {
    detail = detail.trim();
    if (!detail) { return; }
    this.pictureService.addPicture({ detail } as Picture)
      .subscribe(picture => {
        this.pictures.push(picture);
      });
  } */


  editPicture(picture: Picture): void {
    localStorage.removeItem("editPictureId");
    localStorage.setItem("editPictureId", picture.id.toString());
    this.router.navigate(['edit-picture']);
  };



  delete(picture: Picture): void {
    this.pictures = this.pictures.filter(h => h !== picture);
    this.pictureService.deletePicture(picture).subscribe();
  }

  goBack() :void {
    this.location.back();
  }

}


  

