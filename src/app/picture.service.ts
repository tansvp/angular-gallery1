import { Injectable } from '@angular/core';

import { Picture } from './picture'; /* ตัวประกาศ */

import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';



@Injectable({ providedIn: 'root' })

export class PictureService {

  
  
    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
      };
    

    constructor(
      private http: HttpClient,
     
      /* private messageService: MessageService */) { }

      picturesUrl='http://localhost:8080/api/pictures';  /* เพิ่ม */

     
     /** GET pictures from the server */
  getPictures (): Observable<Picture[]> {
    return this.http.get<Picture[]>(this.picturesUrl)
      /* .pipe(
        tap(_ => this.log('fetched pictures')),
        catchError(this.handleError<Picture[]>('getPictures', []))
      ); */
  }

/*  getPictureNo404<Data>(id: number): Observable<Picture> {
    const url =  `${this.picturesUrl}/?id=${id}`;
    return this.http.get<Picture[]>(url)
      .pipe(
        map(pictures => pictures[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? 'fetched' : 'did not find';
          this.log(`${outcome} picture id=${id}`);
        }),
        catchError(this.handleError<Picture>(`getPicture id=${id}`))
      );
  }*/  

/*  getPicture(id: number): Observable<Picture> {
    const url = `${this.picturesUrl}`;
    return this.http.get<Picture>(url).pipe(
      tap(_ => this.log(`fetched picture id=${id}`)),
      catchError(this.handleError<Picture>(`getPicture id=${id}`))
    );
  }
 */ 

/* searchPictures(term: string): Observable<Picture[]> {
    if (!term.trim()) {
      // if not search term, return empty picture array.
      return of([]);
    }
    return this.http.get<Picture[]>(`${this.picturesUrl}/?name=${term}`).pipe(
      tap(x => x.length ?
         this.log(`found pictures matching "${term}"`) :
         this.log(`no pictures matching "${term}"`)),
      catchError(this.handleError<Picture[]>('searchPictures', []))
    );
  } */
 

/*  addPicture (picture: Picture): Observable<Picture> {
    return this.http.post<Picture>(this.picturesUrl, picture, this.httpOptions).pipe(
      tap((newPicture: Picture) => this.log(`added picture w/ id=${newPicture.id}`)),
      catchError(this.handleError<Picture>('addPicture'))
    );
  }
 */
 


  deletePicture (picture: Picture | number): Observable<Picture> {
    const id = typeof picture === 'number' ? picture : picture.id;
    const url = `${this.picturesUrl}/${id}`;

    return this.http.delete<Picture>(url, this.httpOptions)/* .pipe(
      tap(_ => this.log(`deleted picture id=${id}`)),
      catchError(this.handleError<Picture>('deletePicture'))
    ); */
  }

  /*  updatePicture (picture: Picture): Observable<any> {
    return this.http.put(this.picturesUrl, picture, this.httpOptions).pipe(
      tap(_ => this.log(`updated picture id=${picture.id}`)),
      catchError(this.handleError<any>('updatePicture'))
    ); */

  



  /* private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  } */

 /*  private log(message: string) {
    this.messageService.add(`PictureService: ${message}`);
  }
 */



getPictureById(id: number) {
  return this.http.get<Picture>(this.picturesUrl + '/' + id);
}


createPicture(picture: Picture) {
  return this.http.post(this.picturesUrl, picture);
}

updatePicture(picture: Picture) {
  return this.http.put(this.picturesUrl + '/' + picture.id, picture);
}

/* เพิ่ม */


}
