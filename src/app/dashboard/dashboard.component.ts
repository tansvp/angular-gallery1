import { Component, OnInit } from '@angular/core';
import { Picture } from '../picture';
import { PictureService } from '../picture.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {
  pictures: Picture[] = [];

/* SEA&SKY */
myimage50:string = "assets/image/50.jpg"; 
myimage51:string = "assets/image/51.jpg";
myimage52:string = "assets/image/52.jpg";
myimage53:string = "assets/image/1.3.jpg";
myimage54:string = "assets/image/54.jpg";
myimage55:string = "assets/image/55.jpg";  


myimage1:string = "assets/image/1.1.jpg"; 
myimage2:string = "assets/image/1.6.jpg";
myimage3:string = "assets/image/6.1.jpg";
myimage4:string = "assets/image/3.22.jpg";
myimage5:string = "assets/image/1.5.jpg";




  constructor(private pictureService: PictureService) { }

  ngOnInit() {
    this.getPictures();
  }

  getPictures(): void {
    this.pictureService.getPictures()
      .subscribe(pictures => this.pictures = pictures.slice(1, 5));
  }
}