/* import { Component, OnInit } from '@angular/core';

import { Observable, Subject } from 'rxjs';

import {debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

import { Picture } from '../picture';
import { PictureService } from '../picture.service';

@Component({
  selector: 'app-picture-search',
  templateUrl: './picture-search.component.html',
  styleUrls: [ './picture-search.component.css' ]
})
export class PictureSearchComponent implements OnInit {
  pictures$: Observable<Picture[]>;
  private searchTerms = new Subject<string>();

  constructor(private pictureService: PictureService) {}

  // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.pictures$ = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),

      // ignore new term if same as previous term
      distinctUntilChanged(),

      // switch to new search observable each time the term changes
      switchMap((term: string) => this.pictureService.searchPictures(term)),
    );
  }
} */