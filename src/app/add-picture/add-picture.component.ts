import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, NgForm} from "@angular/forms";
import {PictureService} from "../picture.service";
import {first} from "rxjs/operators";
import {Router} from "@angular/router";
import{Picture} from "../picture";

@Component({
  selector: 'app-add-picture',
  templateUrl: './add-picture.component.html',
  styleUrls: ['./add-picture.component.css']
})
export class AddPictureComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,private router: Router, private pictureService: PictureService) { }

  addForm: FormGroup;

  ngOnInit() {

    this.addForm = this.formBuilder.group({   /* ส่วนทีทำให้ขึ้นมาหลัง Add */
      id: [],
      detail: ['', Validators.required],
      name: ['', Validators.required],
      camera:['', Validators.required],
    });

  }

  SubmitAdd(pictureFrom: NgForm){
    if(pictureFrom.invalid){
      return;
    }
    const values = pictureFrom.value;
    alert(JSON.stringify(values));
    let picture = new Picture();
    picture.name = values.name;
    picture.detail = values.detail;
    picture.camera = values.camera;

    this.pictureService.createPicture(picture)
    .subscribe(data=>{
      alert("Add new picture complete!!");
      this.router.navigate(['pictures']);
    });
    error=>{
      alert(error.error.message)
    }
  }

}
