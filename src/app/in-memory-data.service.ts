/* 
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Picture } from './picture';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const pictures = [

      {
        id:1,
        detail: ' The Sun does not realise how wonderful it is until after a room is made.',
        name:'Sky&Sea',
        camera : 'Fujifilm X-T20'
      },
    
      {
        id:2,
        detail: ' Sometimes the dreams that come true are the dreams you never even knew you had.',
        name:'Sky&Sea',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:3,
        detail: ' Remember the complements you receive. Forget the insults.',
        name:'Sky&Sea',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:4,
        detail: ' The secret of creativity is knowing how to hide your sources.',
        name:'Sky&Sea',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:5,
        detail: ' Always remember: too much EGO will kill your talent.',
        name:'Sky&Sea',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:6,
        detail: ' Nothing great was ever achieved without enthusiasm.',
        name:'Sky&Sea',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:7,
        detail: ' It’s never too late to start again.',
        name:'Moon',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:8,
        detail: 'Never let go of your dream.',
        name:'Moon',
        camera : 'Fujifilm X-T20'
    
    
      },
    
      {
        id:9,
        detail: 'An essential aspect of creativity is not being afraid to fail.',
        name:'Moon',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:10,
        detail: ' Lose your dreams and you will lose your mind.',
        name:'Moon',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:11,
        detail: 'Our greatest glory is not in never falling,but in rising every time we fall.',
        name:'Moon',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:12,
        detail: ' Every day may not be good,but there is something good in every day.',
        name:'Mountain',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:13,
        detail: 'If you want to make your dreams come true,the first thing you have to do is WAKE UP.',
        name:'Mountain',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:14,
        detail: " Life isn't about finding yourself. Life is about creating yourself.",
        name:'Mountain',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:15,
        detail: 'Laugh as much as you breath and love as much as you live.',
        name:'Mountain',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:16,
        detail: 'The time you enjoy wasting is not wasted time.',
        name:'Mountain',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:17,
        detail: 'If life was easy,where would all the adventures be?',
        name:'Mountain',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:18,
        detail: 'Stop thinking of what could go wrong and start thinking of what could go right.',
        name:'Museum',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:19,
        detail: 'Action is the foundational key to all success.',
        name:'Museum',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:20,
        detail: ' When I’m working on a problem, I never think about beauty.But when I’ve finished, if the solution is not beautiful I know it’s wrong',
        name:'Museum',
        camera : 'Fujifilm X-T20'
    
      },
    
    
      {
        id:21,
        detail: ' You can get everything in life you want,If you just help enough other people get what they want.',
        name:'Museum',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:22,
        detail: 'Simplicity is the ultimate sophistication.',
        name:'Museum',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:23,
        detail: 'The most successful people in the world Have made many mistakes. And experienced far more failure than the rest.',
        name:'Museum',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:24,
        detail: "Never give up on something you really want.However impossible things my seem,There's always a way.",
        name:'Museum',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:25,
        detail: 'You must lose your fear of being wrong, In order to live a creative life.',
        name:'Temple',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:26,
        detail: "Do not allow what you cannot do,Interfere with what you can do. You can reach your dreams faster, Doing things you're good at.",
        name:'Temple',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:27,
        detail: 'Living a false life will only lead to distress and worries.Be yourself, everyone is already taken.',
        name:'Temple',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:28,
        detail: 'Never miss the opportunity to follow your dreams. Older people who are unhappy with life, Are those with regrets.',
        name:'Temple',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:29,
        detail: "Don't look back, walk forward. You will know what success is like.",
        name:'Temple',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:30,
        detail: 'Don’t let what other people think, stop you from doing the things you love.',
        name:'Temple',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:31,
        detail: 'Who hurt you, you give affection toward them is better than hurting them back.',
        name:'Temple',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:32,
        detail: ' Not all storms come to disrupt your life,some come to clear your path.',
        name:'Temple',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:33,
        detail: 'Meditation can calm your mind.',
        name:'Temple',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:34,
        detail: "  Work hard today is better than work harder tomorrow.",
        name:'Temple',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:35,
        detail: " Life isn't as easy as it seems,but its not as difficult as we think.",
        name:'Museum',
        camera : 'Fujifilm X-T20'
    
      },
    
    
      {
        id:37,
        detail: 'Knowing greed, one will learn to have enough🖐 knowing life, one will be selective.',
        name:'Museum',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:38,
        detail: 'Thinking of relaxing, think of meditation.',
        name:'Museum',
        camera : 'Fujifilm X-T20'
    
      },
    
      {
        id:36,
        detail: 'Whatever you decide to do, make sure it makes you happy.',
        name:'Museum',
        camera : 'Fujifilm X-T20'
    
      },
    
     
 



    ];
    return {pictures};
  }

  // Overrides the genId method to ensure that a picture always has an id.
  // If the Pictures array is empty,
  // the method below returns the initial number (11).
  // if the Pictures array is not empty, the method below returns the highest
  // picture id + 1.
  genId(pictures: Picture[]): number {
    return pictures.length > 0 ? Math.max(...pictures.map(picture => picture.id)) + 1 : 11;
  }
} */



