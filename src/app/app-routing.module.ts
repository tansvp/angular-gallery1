
 import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {AddPictureComponent} from "./add-picture/add-picture.component";
import {EditPictureComponent} from "./edit-picture/edit-picture.component";

import { PicturesComponent }      from './pictures/pictures.component';

import { RegisterComponent } from './register/register.component';
/* import { PictureDetailComponent }  from './picture-detail/picture-detail.component'; */
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
/* import { LogoutComponent } from './logout/logout.component'; */




const routes: Routes = [
  { path: '', redirectTo: '/pictures', pathMatch: 'full' },   /* หน้าเริ่มต้น */

/*   { path: 'detail/:id', component: PictureDetailComponent }, 
 */
  { path: 'pictures', component: PicturesComponent },
  { path: 'add-picture', component: AddPictureComponent },
  { path: 'edit-picture', component: EditPictureComponent },
  {  path: 'register',component: RegisterComponent},
  {  path: 'login',component: LoginComponent},
  { path : 'dashboard' ,component: DashboardComponent},
  { path: 'profile', component: ProfileComponent },
  /* { path : 'logout',component: LogoutComponent}, */

];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
 