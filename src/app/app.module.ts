import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TopBarComponent } from './top-bar/top-bar.component';
import { AppComponent } from './app.component';
import { PicturesComponent } from './pictures/pictures.component';

/*import { PictureDetailComponent } from './picture-detail/picture-detail.component';  */

/* import { MessagesComponent } from './messages/messages.component'; */
import { RouterModule } from '@angular/router'
import { ReactiveFormsModule } from '@angular/forms'
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AppRoutingModule }     from './app-routing.module';

/* import { PictureSearchComponent }  from './picture-search/picture-search.component';
 */import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

/*import { InMemoryDataService }  from './in-memory-data.service';  */


/* import { AlertComponent } from './_components'; */
/* import { HomeComponent } from './home'; */
/* import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { fakeBackendProvider } from './_helpers'; */
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import {AddPictureComponent} from "./add-picture/add-picture.component";
import {EditPictureComponent} from "./edit-picture/edit-picture.component";
import { PictureService } from './picture.service';

import {DashboardComponent } from "./dashboard/dashboard.component";

/* import {LogoutComponent} from "./logout/logout.component" */
import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { ProfileComponent } from './profile/profile.component';



@NgModule({
  declarations: [
    AppComponent,
    PicturesComponent,

    TopBarComponent,
    /* MessagesComponent, */
    RegisterComponent,
    LoginComponent,
    /*      PictureSearchComponent,
*/
    /* AlertComponent, */
    /* HomeComponent, */
    AddPictureComponent,
    EditPictureComponent,
    DashboardComponent,
    ProfileComponent,
    /* LogoutComponent, */

    
  ],
 
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,


    /*    RouterModule.forRoot([
      {  path: 'pictures',component:PicturesComponent},
      {  path: 'register',component: RegisterComponent},
      {  path: 'login',component: LoginComponent}]),
      HttpClientInMemoryWebApiModule.forRoot(
     
      InMemoryDataService, { dataEncapsulation: false } 
     

  )
      */
 
   
    HttpClientModule,
    ],
    

    /*  */

    providers: [ PictureService ,
      authInterceptorProviders],


  bootstrap: [AppComponent]
})

export class AppModule {}
